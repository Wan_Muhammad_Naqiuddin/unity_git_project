﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScript : MonoBehaviour {
    public float bulletSpeed;
    public Rigidbody bulletRB;
    public GameObject explosion;
    public GameObject enemyBullet;
    public AudioSource boom;

    // Use this for initialization
    void Start () {

        bulletRB = GetComponent<Rigidbody>();
        bulletRB.velocity= transform.forward * bulletSpeed;
        //Destroy(gameObject);
	}
	
	// Update is called once per frame
	void Update () {

        Destroy(gameObject, 2f);
	}

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Asteroid")
        {
            Instantiate(explosion, other.transform.position, other.transform.rotation);
            Destroy(other.gameObject);
            Score asteroidBullet;
            asteroidBullet = GameObject.Find("Player").GetComponent<Score>();
            asteroidBullet.asteroidsDestroyed += 1;
        }

        if (other.gameObject.tag == "Enemy")
        {
            Instantiate(explosion, other.transform.position, other.transform.rotation);
            Destroy(other.gameObject);
            Score enemyBullet;
            enemyBullet = GameObject.Find("Player").GetComponent<Score>();
            enemyBullet.enemyshipsDestroyed += 1;

        }

      
    }

   
}
