﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraAnchor : MonoBehaviour {

    public float speed;
    private float timeLimit = 10f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        if (Time.time >= timeLimit)
        {
            speed += 1f;
            timeLimit += 10f;
        }

        transform.Translate(0f, 1f* speed * Time.deltaTime, 0f);


    }
}
