﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyScript : MonoBehaviour {
    public float startTimeBtwShots;
    private float timeBtwShots;
    public GameObject bullet;
    private Transform Player;
    public GameObject muzzle;
	// Use this for initialization
	void Start () {
        timeBtwShots = startTimeBtwShots;
       
	}
	
	// Update is called once per frame
	void Update () {
        if (timeBtwShots <= 0)
        {
            //Instantiate(bullet, muzzle.transform.position, Quaternion.identity);
            Instantiate(bullet, muzzle.transform.position, muzzle.transform.rotation);

            timeBtwShots = startTimeBtwShots;
        }
        else
        {
            timeBtwShots -= Time.deltaTime;
        }
	}
}
