﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectSpawner : MonoBehaviour {

    public GameObject[] Obstacleprefab;
    private GameObject Shooter;

    public float SpawnInterval = 1;
    private float NextSpawn = 0;

    public Vector3 center;
    public Vector3 size;
    private int index;


	// Use this for initialization
	void Start ()
    {
        //SpawnObstacle();
	}
	
	// Update is called once per frame
	void Update () {
        center = transform.position;

        if (Time.time >= NextSpawn)
        {
            NextSpawn = Time.time + SpawnInterval;
            SpawnObstacle();
        }
	}

    public void SpawnObstacle()
    {
       
        Vector3 pos = center + new Vector3(Random.Range(-size.x / 2, size.x / 2), Random.Range(-size.y / 2, size.y / 2), Random.Range(-size.z / 2, size.z / 2));

        

       
        index = Random.Range(0, Obstacleprefab.Length);
        Shooter = Obstacleprefab[index];
        Instantiate(Shooter, pos, Quaternion.identity);

    }
     void OnDrawGizmosSelected()
    {
        Gizmos.color = new Color(1, 0, 0, 0.5f);
        Gizmos.DrawCube(center, size);
    }
}
