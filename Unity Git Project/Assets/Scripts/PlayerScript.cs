﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerScript : MonoBehaviour {

    public float speed;
    private Rigidbody rb;
    public int health = 100;
    public float tilt = 1f;
    public GameObject explosion;
    public GameObject playerLaserBeam;
    public int ammo;
    public int firerate;
    public Slider HealthBar;
    public Slider AmmoBar;
    public GameObject cannonMuzzle1;
    public GameObject cannonMuzzle2;
    public GameObject LoseUI;
    public AudioSource boom;

	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody>();
		
	}
	
	// Update is called once per frame
	void Update () {

        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0f, moveVertical);

        rb.AddForce(movement * speed);

        rb.rotation = Quaternion.Euler(0.0f, 0.0f, rb.velocity.x * -tilt); 
        if (health <= 0)
        {
            Destroy(gameObject);
            LoseUI.SetActive(true);
        }

        if (Input.GetButton("Fire1") && ammo>=0)
        {
            Fire();
            ammo -= 1;
            AmmoBar.value -= 1;
        }


        //transform.Translate(Input.GetAxis("Horizontal") *speed* Time.deltaTime, 0f, 0f);
        //transform.Translate(0f, 0f, Input.GetAxis("Vertical") * speed * Time.deltaTime);

       

    }
    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "Asteroid")
        {
            health -= 10;
            HealthBar.value -= 10;
            Instantiate(explosion, collision.transform.position, collision.transform.rotation);
            Destroy(collision.gameObject);
            boom.Play();
            print("audio");
            Score asteroidScore;
            asteroidScore = GameObject.Find("Player").GetComponent<Score>();
            asteroidScore.asteroidsDestroyed += 1;
        }

        if (collision.gameObject.tag == "Enemy")
        {
            health -= 10;
            HealthBar.value -= 10;
            Instantiate(explosion, collision.transform.position, collision.transform.rotation);
            boom.Play();
            Destroy(collision.gameObject);
            Score enemyScore;
            enemyScore = GameObject.Find("Player").GetComponent<Score>();
            enemyScore.enemyshipsDestroyed += 1;

        }

        if (collision.gameObject.tag == "Ammo")
        {
            ammo += 10;
            AmmoBar.value += 10;
            Destroy(collision.gameObject);
        }

        if (collision.gameObject.tag == "EnemyBullet")
        {
            health -= 10;
            HealthBar.value -= 10;
            Destroy(collision.gameObject);
        }
    }

    public void Fire()
    {
        Instantiate(playerLaserBeam, cannonMuzzle1.transform.position, cannonMuzzle1.transform.rotation);
        Instantiate(playerLaserBeam, cannonMuzzle2.transform.position, cannonMuzzle2.transform.rotation);
    }

    
}





