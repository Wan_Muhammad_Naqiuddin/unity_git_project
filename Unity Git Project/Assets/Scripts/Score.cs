﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour {

    public int asteroidsDestroyed;
    public int enemyshipsDestroyed;
    public Text AsteroidsDestroyed;
    public Text EnemyshipsDestroyed;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        AsteroidsDestroyed.text = "Asteroids Destroyed: " + asteroidsDestroyed;
        EnemyshipsDestroyed.text = "Enemy Ships Destroyed: " + enemyshipsDestroyed;
	}
}
